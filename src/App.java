import EmployeeClass.EmployeeClass;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        EmployeeClass employee1 = new EmployeeClass(1, "nguyen", "cuong", 20000000);
        EmployeeClass employee2 = new EmployeeClass(2, "nguyen", "du", 100000000);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        System.out.println(employee1.getName());
        System.out.println(employee2.getName());

        System.out.println(employee1.getAnnualSalary());
        System.out.println(employee2.getAnnualSalary());

        System.out.println(employee1.raiseSalary(30f));
        System.out.println(employee2.raiseSalary(50f));
    }
}
